FROM python:3.8

RUN mkdir /app
WORKDIR /app

COPY projet_python.tar.gz .
RUN  tar xzf projet_python.tar.gz

WORKDIR /app/projet_python
RUN pip install --no-cache-dir -r requirements.txt
CMD python app.py